function palindrome(kata) {
    var reverseKata = [];
    for (i in kata) {
        reverseKata.unshift(kata[i]);
    }
    return (reverseKata.join('') == kata) ? true : false;
}

// TEST CASES
console.log(palindrome('katak')); // true
console.log(palindrome('blanket')); // false
console.log(palindrome('civic')); // true
console.log(palindrome('kasur rusak')); // true
console.log(palindrome('mister')); // false