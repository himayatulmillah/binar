let firstChild = document.getElementById("eldest-parent").children;
firstChild[0].innerHTML = "Diakses Melalui Eldest Parent";

let youngerChild = document.getElementById("a-child");
youngerChild.previousElementSibling.innerHTML = "Diakses Melalui a Child";
youngerChild.nextElementSibling.innerHTML = "Diakses Melalui a Child";

let olderChild = document.getElementById("a-child").parentNode.parentNode.nextElementSibling;
olderChild.innerHTML = "Diakses Melalui a Child";