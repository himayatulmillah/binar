
/**
 * 
 * Diberikan function ubahHuruf(kata) yang akan menerima satu parameter berupa string.
 * Function akan me-return sebuah kata baru dimana setiap huruf akan digantikan dengan huruf alfabet setelahnya.
 * Contoh, huruf a akan menjadi b, c akan menjadi d, k menjadi l, dan z menjadi a.
 * 
 */


function ubahHuruf(kata) {
    // you can only write your code here!
    let newKata = "";
    for (idx in kata) {
        let charCode = kata.charCodeAt(idx) + 1;
        if (charCode > 122) {
            charCode = 97;
        }
        let str = String.fromCharCode(charCode);
        newKata += str;
    }
    return newKata;
}
  
// TEST CASES
console.log(ubahHuruf('wow')); // xpx
console.log(ubahHuruf('developer')); // efwfmpqfs
console.log(ubahHuruf('javascript')); // kbwbtdsjqu
console.log(ubahHuruf('keren')); // lfsfo
console.log(ubahHuruf('semangat')); // tfnbohbu
